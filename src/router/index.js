import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import("../views/HomeView.vue"),
      redirect: "/index",
      children: [
        {
          path: "/index",
          name: "index",
          component: () => import("../views/index.vue"),
        },
        {
          path: '/header',
          name: 'header',
          component: () => import('../components/header.vue')
        },{
          path: '/footer',
          name: 'footer',
          component: () => import('../components/footer.vue')
        },{
          path: '/404',
          name: '404',
          component: () => import('../views/404.vue')
        },{
          path: '/Give',
          name: 'Give',
          component: () => import('../views/Give.vue')
        },{
          path: '/About',
          name: 'About',
          component: () => import('../views/About.vue')
        },{
          path: '/GiveService',
          name: 'GiveService',
          component: () => import('../views/GiveService.vue')
        },{
          path: '/blog',
          name: 'blog',
          component: () => import('../views/blog.vue')
        },{
          path: '/Gallery',
          name: 'Gallery',
          component: () => import('../views/Gallery.vue')
        },{
          path: '/Patient',
          name: 'Patient',
          component: () => import('../views/Patient.vue')
        },{
          path: '/Contact',
          name: 'Contact',
          component: () => import('../views/Contact.vue')
        },{
          path: '/Test',
          name: 'Test',
          component: () => import('../views/Test.vue')
        }
      ],
    },
  ]
})

export default router
